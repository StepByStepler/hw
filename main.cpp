#include <iostream>
#include <vector>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
// SquadInterface.h

inline size_t maximum_steps = 10;

struct Warrior {
    string name;
    int    health;
    int    defense;
    int    dodge;
    int    strength;
    int    dexterity;
};

typedef vector<Warrior>     Squad;
typedef vector<Squad>       Scene;
typedef pair<size_t,size_t> WarriorAddress_SW;

class IRules {
public:
    virtual ~IRules() = default;

    virtual bool                      isDone            (const Scene & scene) const               = 0;
    virtual vector<WarriorAddress_SW> generateStepsQueue(const Scene & scene) const               = 0;
    virtual void                      doStep            (WarriorAddress_SW warrior_address, Scene & scene) const = 0;
};

class IOutput {
public:
    virtual ~IOutput() = default;

    virtual void initializeScene(const Scene & scene) = 0;
    virtual void displayStepResults(const WarriorAddress_SW warrior_address, const Scene & scene) = 0;
    virtual void displayRoundResults(const Scene & scene) = 0;
    virtual void finilizeScene(const Scene & scene) = 0;
};

class IFight {
public:
    virtual ~IFight() = default;

    virtual void doFight(Scene & , const IRules & , IOutput & ) = 0;
};

////////////////////////////////////////////////////////////////////////////////
// SimpleFight.h

class SimpleFight : public IFight {
public:
    virtual void doFight(Scene & scene, const IRules & rules, IOutput & out) override;
};

////////////////////////////////////////////////////////////////////////////////
// SimpleFight.cpp

void SimpleFight::doFight(Scene & scene, const IRules & rules, IOutput & out) {

    out.initializeScene(scene);

    while(true) {
        if (rules.isDone(scene))
            return;

        vector<WarriorAddress_SW> steps_queue = rules.generateStepsQueue(scene);

        for(const WarriorAddress_SW warrior_address : steps_queue) {
            rules.doStep(warrior_address, scene);
            out.displayStepResults(warrior_address, scene);
        }

        out.displayRoundResults(scene);
    }

    out.finilizeScene(scene);
}

////////////////////////////////////////////////////////////////////////////////
// SimpleRules.h

class SimpleRules : public IRules {
public:
    virtual bool                      isDone            (const Scene & scene) const override;
    virtual vector<WarriorAddress_SW> generateStepsQueue(const Scene & scene) const override;
    virtual void                      doStep            (WarriorAddress_SW warrior_address, Scene & scene) const override;
};

////////////////////////////////////////////////////////////////////////////////
// SimpleRules.cpp

bool SimpleRules::isDone (const Scene & scene) const {
    static size_t steps_count = 0;

    if (steps_count >= maximum_steps)
        return true;

    steps_count ++;

    int ready_squad_quantity = scene.size();

    for(const Squad & squad : scene) {
        int ready_warrior_quantity = squad.size();

        for(const Warrior & warrior : squad)
            if (warrior.health <= 0)
                ready_warrior_quantity--;

        if (ready_warrior_quantity == 0)
            ready_squad_quantity --;
    }

    return ready_squad_quantity < 2;
}

vector<WarriorAddress_SW> SimpleRules::generateStepsQueue(const Scene & scene) const {
    vector<WarriorAddress_SW> res;

    for(size_t squad_index=0; squad_index < scene.size(); squad_index++)
        for(size_t warrior_index=0; warrior_index < scene[squad_index].size(); warrior_index++)
            if (scene[squad_index][warrior_index].health > 0)
                res.push_back({squad_index,warrior_index});

    return res;
}

void SimpleRules::doStep(WarriorAddress_SW warrior_address, Scene & scene) const {
    auto [i_squad_index, i_warrior_index] = warrior_address;

    if (scene[i_squad_index][i_warrior_index].health <= 0)
        return;

    for(size_t is=0; is < scene.size(); is++)
        if (is != i_squad_index)
            for(size_t iw=0; iw < scene[is].size(); iw++)
                if (scene[is][iw].health > 0) {
                    int damage = scene[is][iw].defense + scene[is][iw].dodge -
                            scene[i_squad_index][i_warrior_index].strength -
                            scene[i_squad_index][i_warrior_index].dexterity;
                    if (damage > 0)
                        scene[is][iw].health -= damage;
                    return;
                }
    return;
}

////////////////////////////////////////////////////////////////////////////////
// ConsoleOutput.h

class ConsoleOutput : public IOutput {
    Scene   _last_scene;

public:
    virtual void initializeScene(const Scene & scene) override;
    virtual void displayStepResults(const WarriorAddress_SW warrior_address, const Scene & scene) override;
    virtual void displayRoundResults(const Scene & scene) override;
    virtual void finilizeScene(const Scene & scene) override;
};

////////////////////////////////////////////////////////////////////////////////
// ConsoleOutput.cpp

void ConsoleOutput::initializeScene(const Scene & scene) {
    _last_scene = scene;
    displayRoundResults(scene);
}

void ConsoleOutput::displayStepResults(const WarriorAddress_SW warrior_address, const Scene & scene) {
    auto [i_squad_index, i_warrior_index] = warrior_address;

    cout << "\tходит " << scene[i_squad_index][i_warrior_index].name << ": ";

    for(size_t is=0; is < scene.size(); is++)
        for(size_t iw=0; iw < scene[is].size(); iw++)
            if (_last_scene[is][iw].health != scene[is][iw].health) {
                cout << " повредил '" << scene[is][iw].name
                     << "' на " << (_last_scene[is][iw].health - scene[is][iw].health) << ", ";
            }

    cout << endl;

    _last_scene = scene;
}

void ConsoleOutput::displayRoundResults(const Scene & scene) {
    for(size_t is=0; is < scene.size(); is++) {
        cout << "отряд " << is+1 << ": ";
        for(size_t iw=0; iw < scene[is].size(); iw++)
            cout << scene[is][iw].name << "(" << scene[is][iw].health << ") ";
        cout << endl;
    }

    _last_scene = scene;
}

void ConsoleOutput::finilizeScene(const Scene & scene) {
    displayRoundResults(scene);
    cout << "Done!" << endl;
}

////////////////////////////////////////////////////////////////////////////////
// main.cpp

int main()
{
    SimpleRules simple_rules;
    SimpleFight simple_fight;
    ConsoleOutput cout_output;

    Scene       scene {
        {
            // name, health defense dodge strength dexterity
            {"рыцарь", 100, 80,  5, 20,  5},
            {"вор",     50, 40, 50, 20, 40},
            {"лучник",  50, 60, 30, 30, 20},
        },
        {
            {"троль",  200, 80,  5, 30,  5},
            {"маленький троль",  100, 60,  5, 10,  15},
        },
    };

    simple_fight.doFight(scene, simple_rules, cout_output);

    return 0;
}
